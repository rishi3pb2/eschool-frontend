import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import actions from "../redux/actions";
import "./style.css";
import { Form, Button, Container, Spinner, Row, Col } from "react-bootstrap";

const Signin = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const history = useHistory();
  const handleLoginClick = (e) => {
    e.preventDefault();
    dispatch(actions.login(email, password));
  };
  const { loading, userInfo } = useSelector((state) => state.LoginReducer);

  useEffect(() => {
    if (userInfo) {
      history.push("/dashboard");
    }
  }, [dispatch, history, userInfo]);

  return (
    <Container className="myContainerClass">
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Remember me" />
        </Form.Group>
        <Row>
          <Col xs={2}>
            <Button
              variant="warning"
              type="submit"
              onClick={(e) => handleLoginClick(e)}
            >
              {loading ? (
                <Spinner animation="border" variant="danger" size="sm" />
              ) : (
                "Login"
              )}
            </Button>
          </Col>
          <Col xs={1}>
            <div>or</div>
          </Col>
          <Col xs={4}>
            <Button variant="danger" type="submit" className="ml-2">
              <i class="fab fa-google"></i>{" "}
              <a href="http://localhost:3333/api/auth/google">Login</a>
            </Button>
          </Col>
        </Row>
        <Form.Text className="text-muted mt-2">
          Don't have a account? <Link to="/signup">Signup</Link>
        </Form.Text>
      </Form>
    </Container>
  );
};

export default Signin;
