import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { AddAssignmentModal } from "../components/modals";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { useHistory } from "react-router";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function createData(id, name, type, start_date, end_date, submissions) {
  return { id, name, type, start_date, end_date, submissions };
}

const rows = [
  createData(1, "Minor", "Test", "2021-10-31", "2021-11-01", 2),
  createData(2, "Major", "Test", "2021-10-31", "2021-11-01", 2),
  createData(3, "Axios", "Assignment", "2021-10-31", "2021-11-01", 2),
  createData(4, "Postulates", "Assignment", "2021-10-31", "2021-11-01", 2),
  createData(5, "Something", "Test", "2021-10-31", "2021-11-01", 2),
  createData(6, "Minor", "Test", "2021-10-31", "2021-11-01", 2),
  createData(7, "Major", "Test", "2021-10-31", "2021-11-01", 2),
];

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

const Assignments = (props) => {
  const [modalShow, setModalShow] = useState(false);
  const [rowData, setRowData] = useState({});
  const history = useHistory();
  const userInfo = JSON.parse(localStorage.getItem("userInfo"));
  const role = userInfo?.role;

  const handleClick = (row) => {
    setRowData(row);
    history.push(`${history.location.pathname}/${row.id}`);
  };

  const classes = useStyles();
  return (
    <>
      {role === "teacher" && (
        <Button
          variant="warning"
          onClick={() => {
            setRowData({});
            setModalShow(true);
          }}
          style={{ margin: "10px 0px 10px 0px" }}
        >
          + Create New Assignment
        </Button>
      )}
      <AddAssignmentModal
        data={rowData}
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
      <TableContainer component={Paper}>
        <Table className={classes.table} stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <StyledTableCell>Name</StyledTableCell>
              <StyledTableCell align="right">Type</StyledTableCell>
              <StyledTableCell align="right">Posted Date</StyledTableCell>
              <StyledTableCell align="right">Deadline</StyledTableCell>
              <StyledTableCell align="right">Submission</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <StyledTableRow
                key={row.name}
                style={{ cursor: "pointer" }}
                onClick={() => handleClick(row)}
              >
                <StyledTableCell component="th" scope="row">
                  {row.name}
                </StyledTableCell>
                <StyledTableCell align="right">{row.type}</StyledTableCell>
                <StyledTableCell align="right">
                  {row.start_date}
                </StyledTableCell>
                <StyledTableCell align="right">{row.end_date}</StyledTableCell>

                <StyledTableCell align="right">
                  {row.submissions}
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default Assignments;
