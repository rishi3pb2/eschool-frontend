import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import actions from "../redux/actions";
import "./style.css";
import { Form, Button, Container, Spinner } from "react-bootstrap";
const Signup = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [namE, setnamE] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");

  const { loading, userInfo } = useSelector((state) => state.LoginReducer);
  const handleSignupClick = (e) => {
    e.preventDefault();
    dispatch(actions.signup(namE, email, password, passwordConfirm));
  };
  useEffect(() => {
    console.log(userInfo);
    if (userInfo) {
      history.push("/dashboard");
    }
  }, [dispatch, history, userInfo]);
  return (
    <Container className="myContainerClass">
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter your name"
            value={namE}
            onChange={(e) => setnamE(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Confirm Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Confirm Password"
            value={passwordConfirm}
            onChange={(e) => setPasswordConfirm(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Remember me" />
        </Form.Group>
        <Button
          variant="warning"
          type="submit"
          onClick={(e) => handleSignupClick(e)}
        >
          {loading ? (
            <Spinner animation="border" variant="danger" size="sm" />
          ) : (
            "Signup"
          )}
        </Button>
        <Button variant="danger" type="submit" className="ml-2">
          <i class="fab fa-google"></i> Signup
        </Button>
        <Form.Text className="text-muted mt-2">
          Already have a account? <Link to="/">Login</Link>
        </Form.Text>
      </Form>
    </Container>
  );
};

export default Signup;
