import React, { useState } from "react";
import { Switch, Route, useRouteMatch, NavLink } from "react-router-dom";
import { Container } from "react-bootstrap";
import Calendar from "../components/calendar";
import AssignmentList from "./assignmentList";
import Assignment from "./assignment";
import Result from "./result";
import Student from "./students";

const Subject = () => {
  const { url, path } = useRouteMatch();
  const userInfo = JSON.parse(localStorage.getItem("userInfo"));
  const role = userInfo?.role;
  return (
    <Container style={{ padding: "15px" }}>
      <div className="subjectNavLinks">
        <NavLink
          activeClassName="myActiveLink"
          className="navBox"
          exact
          to={`${url}`}
        >
          Class Schedule
        </NavLink>
        <NavLink
          activeClassName="myActiveLink"
          className="navBox"
          to={`${url}/assignments`}
        >
          Assignment
        </NavLink>
        {role === "teacher" ? (
          <NavLink
            activeClassName="myActiveLink"
            className="navBox"
            to={`${url}/students`}
          >
            Students
          </NavLink>
        ) : (
          <NavLink
            activeClassName="myActiveLink"
            className="navBox"
            to={`${url}/results`}
          >
            Results
          </NavLink>
        )}
      </div>

      <Switch>
        <Route exact path={`${path}/students`}>
          <Student />
        </Route>
        <Route exact path={`${path}/results`}>
          <Result />
        </Route>
        <Route path={`${path}/assignments/:id`}>
          <Assignment />
        </Route>
        <Route exact path={`${path}/assignments`}>
          <AssignmentList />
        </Route>
        <Route exact path={`${path}/`}>
          <Calendar />
        </Route>
      </Switch>
    </Container>
  );
};

export default Subject;
