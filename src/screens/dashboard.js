import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import { Card, Button } from "react-bootstrap";
import "./style.css";
import { Container } from "react-bootstrap";
import { AddSubject } from "../components/modals";
import axios from "axios";

export default function Dashboard() {
  const [modalShow, setModalShow] = useState(false);
  const [subjects, setSubjects] = useState([]);
  const history = useHistory();
  const userInfo = JSON.parse(localStorage.getItem("userInfo"));
  const role = userInfo?.role;
  const getSubjects = async () => {
    try {
      const { token } = JSON.parse(localStorage.getItem("userInfo"));

      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      };

      const {
        data: { data },
      } = await axios.get("http://localhost:3333/api/teacher/subject", config);

      setSubjects(data.data);
      toast.success("Subject list fetched");
    } catch (error) {
      toast.error("Failed to fetch subjects");
    }
  };

  useEffect(() => {
    if (!userInfo) {
      history.push("/");
    }
    getSubjects();
  }, [history, userInfo]);

  return (
    <>
      <Container style={{ paddingLeft: "2em" }}>
        {role === "teacher" && (
          <Button
            variant="warning"
            onClick={() => setModalShow(true)}
            style={{ margin: "10px 10px 10px 0px" }}
          >
            + Add New Subject
          </Button>
        )}
      </Container>

      <AddSubject show={modalShow} onHide={() => setModalShow(false)} />
      <Container
        style={{
          padding: "1em",
          display: "flex",
          flexWrap: "wrap",
          justifyContent: "space-between",
        }}
      >
        {subjects?.map((el) => {
          return (
            <Link exact to={`/subject/${el._id}`} key={el._id}>
              <Card style={{ width: "16rem", margin: "1em" }} key={el._id}>
                <Card.Img
                  variant="top"
                  src="https://blog.careerlauncher.com/wp-content/uploads/2021/06/maths-chalkboard_23-2148178220.jpg"
                />
                <Card.Body>
                  <Card.Title>{el.name}</Card.Title>
                  <Card.Text>{el.description}</Card.Text>
                </Card.Body>
              </Card>
            </Link>
          );
        })}
      </Container>
    </>
  );
}
