import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { AddStudentModal } from "../components/modals";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function createData(name, email) {
  return { name, email };
}

const rows = [
  createData("Rishi Kumar", "rishi@eschool.com"),
  createData("Jay Gupta", "jay@eschool.com"),
];

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

const Student = (props) => {
  const classes = useStyles();
  const [modalShow, setModalShow] = useState(false);
  const userInfo = JSON.parse(localStorage.getItem("userInfo"));
  const role = userInfo?.role;

  return (
    <>
      {role === "teacher" && (
        <Button
          variant="warning"
          onClick={() => setModalShow(true)}
          style={{ margin: "10px 0px 10px 0px" }}
        >
          + Add Students
        </Button>
      )}
      <AddStudentModal show={modalShow} onHide={() => setModalShow(false)} />
      <TableContainer component={Paper}>
        <Table className={classes.table} stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <StyledTableCell>Student Name</StyledTableCell>
              <StyledTableCell>Email</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <StyledTableRow key={row.name}>
                <StyledTableCell component="th" scope="row">
                  {row.name}
                </StyledTableCell>
                <StyledTableCell>{row.email}</StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default Student;
