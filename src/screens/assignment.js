import React from "react";
import {
  Form,
  Card,
  Row,
  Col,
  Nav,
  Button,
  Table,
  Container,
} from "react-bootstrap";
import { NavLink, useRouteMatch, Route, Switch } from "react-router-dom";
const ViewAssignment = () => {
  return (
    <Card.Body>
      <Card>
        <Card.Body>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Assignment Name</Form.Label>
              <Form.Control type="text" placeholder="Assignment Name" />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Deadline</Form.Label>
              <Form.Control type="date" placeholder="Assignment Deadline" />
            </Form.Group>
            <Button className="mb-2">View Last Uploaded Doc</Button>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Update Document</Form.Label>
              <Form.Control type="file" placeholder="assigment.pdf" />
            </Form.Group>
          </Form>
        </Card.Body>
        <Card.Footer>
          <Button variant="warning" type="submit">
            Update
          </Button>
        </Card.Footer>
      </Card>
    </Card.Body>
  );
};

const Submission = () => {
  return (
    <Container className="p-3">
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Submission Time</th>
            <th>Submitted Document</th>
            <th>Assign Score</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
            <td>View</td>
            <td>
              <Row>
                <Col>
                  <Form.Control type="number" placeholder="Score" />
                </Col>
                <Col>
                  <Button className="ml-1" variant="warning">
                    Save
                  </Button>
                </Col>
              </Row>
            </td>
          </tr>
          <tr>
            <td>2</td>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
            <td>View</td>
            <td>
              <Row>
                <Col>
                  <Form.Control type="number" placeholder="Score" />
                </Col>
                <Col>
                  <Button className="ml-1" variant="warning">
                    Save
                  </Button>
                </Col>
              </Row>
            </td>
          </tr>
          <tr>
            <td>3</td>
            <td>Larry the Bird</td>
            <td>Thornton</td>
            <td>@twitter</td>
            <td>View</td>
            <td>
              <Row>
                <Col>
                  <Form.Control type="number" placeholder="Score" />
                </Col>
                <Col>
                  <Button className="ml-1" variant="warning">
                    Save
                  </Button>
                </Col>
              </Row>
            </td>
          </tr>
        </tbody>
      </Table>
    </Container>
  );
};

const Assignment = (props) => {
  const { url, path } = useRouteMatch();

  return (
    <Card style={{ marginTop: "20px" }}>
      <Card.Header style={{ height: "3rem" }}>
        <Nav variant="tabs" defaultActiveKey="">
          <Nav.Item>
            <NavLink
              exact
              className="navLink2"
              activeClassName="myActiveLink2"
              to={`${url}`}
            >
              Assignment
            </NavLink>
          </Nav.Item>
          <Nav.Item>
            <NavLink
              className="navLink2"
              activeClassName="myActiveLink2"
              to={`${url}/submission`}
            >
              Submissions
            </NavLink>
          </Nav.Item>
        </Nav>
      </Card.Header>
      <Switch>
        <Route exact path={`${path}/submission`} component={Submission} />
        <Route path={`${path}`} component={ViewAssignment} />
      </Switch>
    </Card>
  );
};

export default Assignment;
