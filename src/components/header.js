import React from "react";
import "./style.css";
import { Navbar, Container, Nav } from "react-bootstrap";
import { useHistory, NavLink } from "react-router-dom";
import { useDispatch } from "react-redux";
import actions from "../redux/actions";

const Header = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  return (
    <Navbar sticky="top" bg="warning" className="myNavBar">
      <Container>
        <NavLink to="/dashboard">
          <Navbar.Brand>ESCHOOL</Navbar.Brand>
        </NavLink>
        <Nav
          className="me-auto"
          style={{ cursor: "pointer" }}
          onClick={() => dispatch(actions.logout(history))}
        >
          Logout
        </Nav>
      </Container>
    </Navbar>
  );
};

export default Header;
