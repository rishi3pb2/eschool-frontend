import React from "react";
import "./style.css";

const footer = () => {
  return (
    <div
      style={{
        width: "100%",
        position: "absolute",
        bottom: "0",
        marginTop: "20px",
        textAlign: "center",
      }}
    >
      Copyright &copy; Eschool
    </div>
  );
};

export default footer;
