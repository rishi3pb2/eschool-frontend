import React from "react";
import FullCalendar, { formatDate } from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";

const Calendar = () => {
  function renderEventContent(eventInfo) {
    return (
      <>
        <b>{eventInfo.timeText}</b>
        <i>{eventInfo.event.title}</i>
      </>
    );
  }
  const handleDates = (rangeInfo) => {
    console.log(JSON.stringify(rangeInfo));
  };

  const handleEventAdd = (addInfo) => {
    console.log(JSON.stringify(addInfo));
  };

  const handleEventChange = (changeInfo) => {
    console.log(JSON.stringify(changeInfo));
  };

  const handleEventRemove = (removeInfo) => {
    console.log(JSON.stringify(removeInfo));
  };

  const handleEventClick = (clickInfo) => {
    let conf = window.confirm(
      `Are you sure you want to delete the event '${clickInfo.event.title}'`
    );
    if (conf) {
      clickInfo.event.remove(); // will render immediately. will call handleEventRemove
    }
  };
  const handleDateSelect = (selectInfo) => {
    let calendarApi = selectInfo.view.calendar;
    let title = prompt("Please enter a new title for your event");
    calendarApi.unselect();
    if (title) {
      calendarApi.addEvent(
        {
          title,
          start: selectInfo.startStr,
          end: selectInfo.endStr,
          allDay: selectInfo.allDay,
        },
        true
      );
    }
  };

  return (
    <div className="calendar" style={{ background: "#fff", marginTop: "20px" }}>
      <FullCalendar
        plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
        headerToolbar={{
          left: "prev,next today",
          center: "title",
          right: "timeGridWeek,timeGridDay",
        }}
        initialView="timeGridDay"
        editable={true}
        selectable={true}
        selectMirror={true}
        dayMaxEvents={true}
        weekends={true}
        datesSet={handleDates}
        select={handleDateSelect}
        //   events={this.props.events}
        eventContent={renderEventContent} // custom render function
        eventClick={handleEventClick}
        eventAdd={handleEventAdd}
        eventChange={handleEventChange} // called for drag-n-drop/resize
        eventRemove={handleEventRemove}
      />
    </div>
  );
};

export default Calendar;
