import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Signin from "./screens/signin";
import Signup from "./screens/signup";
import Header from "./components/header";
import Dashboard from "./screens/dashboard";
import Subject from "./screens/subject";
// import Footer from "./components/footer";
const App = () => {
  return (
    <div className="App">
      <Router>
        <Header />
        <Switch>
          <Route path="/subject/:id" component={Subject} />
          <Route exact path="/dashboard" component={Dashboard} />
          <Route exact path="/signup" component={Signup} />
          <Route exact path="/" component={Signin} />
        </Switch>
      </Router>
      {/* <Footer /> */}
    </div>
  );
};

export default App;
