import * as types from "../constants";
export const AddSubjectReducer = (state = {}, action) => {
  switch (action.type) {
    case types.ADD_SUBJECT_REQUEST:
      return { loading: true };
    case types.ADD_SUBJECT_SUCCESS:
      return { loading: false, data: action.payload };
    case types.ADD_SUBJECT_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

