import { combineReducers } from "redux";
import { LoginReducer } from "./auth";
import {AddSubjectReducer} from "./teacher"
const rootReducer = combineReducers({ LoginReducer, AddSubjectReducer });

export default rootReducer;
