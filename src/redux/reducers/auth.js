import * as types from "../constants";
export const LoginReducer = (state = {}, action) => {
  switch (action.type) {
    case types.LOGIN_REQUEST:
      return { loading: true };
    case types.LOGIN_SUCCESS:
      return { loading: false, userInfo: action.payload };
    case types.LOGIN_FAIL:
      return { loading: false, error: action.payload };
    case types.SIGNUP_REQUEST:
      return { loading: true };
    case types.SIGNUP_SUCCESS:
      return { loading: false, userInfo: action.payload };
    case types.SIGNUP_FAIL:
      return { loading: false, error: action.payload };
    case types.LOGOUT:
      return {};
    default:
      return state;
  }
};
