import axios from "axios";
import { toast } from "react-toastify";
import * as types from "../constants";
export const addSubject = (name, description) => async (dispatch, getState) => {
  try {
    dispatch({
      type: types.ADD_SUBJECT_REQUEST,
    });

    const { token } = JSON.parse(localStorage.getItem("userInfo"));

    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    };

    const { data } = await axios.post(
      "http://localhost:3333/api/teacher/subject",
      { name, description },
      config
    );

    dispatch({
      type: types.ADD_SUBJECT_SUCCESS,
      payload: data,
    });
    toast.success("Subject addedd");
  } catch (error) {
    toast.error("Failed to add subject");
    dispatch({
      type: types.ADD_SUBJECT_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};