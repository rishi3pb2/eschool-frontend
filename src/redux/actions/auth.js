import * as types from "../constants";
import axios from "axios";
import { toast } from "react-toastify";

export const signup =
  (namE, email, password, passwordConfirm) => async (dispatch) => {
    try {
      dispatch({
        type: types.SIGNUP_REQUEST,
      });
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      const { data } = await axios.post(
        "http://localhost:3333/api/auth/signup",
        { name: namE, email, password, passwordConfirm },
        config
      );
      dispatch({
        type: types.SIGNUP_SUCCESS,
        payload: data,
      });
      toast.success("Signup in");
      localStorage.setItem("userInfo", JSON.stringify(data));
    } catch (error) {
      toast.error("Signup Failed");
      dispatch({
        type: types.SIGNUP_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

export const login = (email, password) => async (dispatch) => {
  try {
    dispatch({
      type: types.LOGIN_REQUEST,
    });

    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    const { data } = await axios.post(
      "http://localhost:3333/api/auth/login",
      { email, password },
      config
    );

    dispatch({
      type: types.LOGIN_SUCCESS,
      payload: data,
    });
    toast.success("Logged in");
    localStorage.setItem("userInfo", JSON.stringify(data));
  } catch (error) {
    toast.error("Login Failed");
    dispatch({
      type: types.LOGIN_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const logout = (history) => async (dispatch) => {
  dispatch({
    type: types.LOGOUT,
  });
  localStorage.removeItem("userInfo");
  history.push("/");
};
