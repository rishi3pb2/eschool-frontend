import { login, logout, signup } from "./auth";
import { addSubject } from "./teacher";

const actions = {
  login: login,
  logout: logout,
  signup: signup,
  addSubject: addSubject
};

export default actions;
